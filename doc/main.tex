%! TeX program = lualatex
\documentclass[
        egregdoesnotlikesansseriftitles,
        paper=a4,
        fontsize=12pt,
        DIV=calc
]{scrarticle}

\usepackage[no-math]{fontspec}
\usepackage{polyglossia}
\setmainlanguage{spanish}

\usepackage{geometry}
 \geometry{
         total={170mm,257mm},
         left=20mm,
         top=20mm,
         bottom=30mm,
 }

\usepackage{siunitx}
\usepackage{etoolbox}
\usepackage[justification=centering]{caption}
\usepackage{subcaption}
\usepackage{multirow,tabularx}

\usepackage{pgfplots}
\usepgfplotslibrary{groupplots,dateplot}
\usetikzlibrary{patterns,shapes.arrows}
\pgfplotsset{compat=newest}
\pgfplotsset{every tick label/.append style={font=\scriptsize}}
\pgfplotsset{every axis label/.append style={font=\scriptsize}}

% Macro to redefine the style of subfigures
\newcommand{\parens}[1]{%
(#1)}
\let\oldsubfigure\thesubfigure
\renewcommand\thesubfigure{\parens{\oldsubfigure}}

\setkomafont{disposition}{\mdseries\rmfamily}
% Different tilte
\makeatletter
\renewcommand*{\@maketitle}{%
        \global\@topnum=\z@
        \setparsizes{\z@}{\z@}{\z@\@plus 1fil}\par@updaterelative
        \ifx\@titlehead\@empty \else
                \begin{minipage}[t]{\textwidth}
                        \usekomafont{titlehead}{\@titlehead\par}%
                \end{minipage}\par
        \fi
        \null
        %\vskip -1em%
        \begin{center}%
                \ifx\@subject\@empty \else
                        {\usekomafont{subject}{\@subject \par}}%
                        \vskip 1em
                \fi
                {\usekomafont{title}{\Huge \@title \par}}%
                \vskip .5em
                {\ifx\@subtitle\@empty\else\usekomafont{subtitle}\@subtitle\par\fi}%
                \vskip 1em
                {%
                        \usekomafont{author}{%
                                \lineskip .5em%
                                \begin{tabular}[t]{c}
                                        \@author
                                \end{tabular}\par
                        \usekomafont{publishers}{%
                                \lineskip .5em%
                                \@publishers\par
                        }%
                }%
        }
        \end{center}%
        \par
        \deffootnote[12pt]{0pt}{0em}{%
                \makebox[9pt][l]{\thefootnotemark}%
        }
}%
\makeatother

% Fonts setup
\defaultfontfeatures[\rmfamily,\sffamily]{Ligatures=TeX}
\setmainfont{TeXGyreTermesX}[
        UprightFont = *-Regular,
        BoldFont = *-Bold,
        ItalicFont = *-Italic,
        BoldItalicFont = *-BoldItalic,
        Extension = .otf]
\usepackage{amsmath}
\setsansfont{LibertinusSans}[
        UprightFont = *-Regular,
        BoldFont = *-Bold,
        ItalicFont = *-Italic,
        Extension = .otf]
\setmonofont{inconsolata}
\usepackage[
        subscriptcorrection,
        nofontinfo,
        zswash,
        mtphrd,
        mtpfrak,
        mtpcal
]{mtpro2}

\usepackage[hypertexnames=false, hidelinks]{hyperref}

\setlength{\parskip}{1em}
\setlength{\parindent}{0em}

% Document information
\author{Juan David Salcedo Hernández}
\title{La expedición geodésica}

\begin{document}
\maketitle

\section{El problema de la forma de la tierra: Newton y Descartes}
Quizá podría decirse que la historia de la expedición geodésica tiene sus
raíces en la rivalidad que existía entre dos de las naciones más influyentes de
Europa para principios del siglo XVII: Gran Bretaña y Francia. Esa rivalidad no se
reducía meramente al ámbito político y militar, sino que en cierta medida se
extendía a la esfera cultural y, más importantemente para esta discusión,
científica.

Precisamente en aquel momento había un debate científico que involucraba dos
visiones, diríase diametralmente opuestas, sobre la geometría de la tierra; aunque
a un nivel más profundo se trataba de una pugna entre dos concepciones sobre la
constitución del universo, la visión Newtoniana y la visión Cartesiana.
Tales modelos no eran precisamente novedades en el momento que se realizó la
expedición.

Los tres volúmenes de \emph{Philosophiae Naturalis Principia
Mathematica}, los Principia de Isaac Newton, fueron publicados en 1687.
En los Principia, Newton dedujo que la tierra debía estar
achatada cerca del ecuador; para comprender cómo llegó a esta conclusión, hay
que conocer una noción que usualmente se enfatiza en los cursos básicos de
física: la distinción entre masa, una medida de la resistencia de un objeto a
ser movido, y peso, una medida de la fuerza gravitacional que la tierra ejerce
sobre él, y que por tanto depende de la distancia entre el objeto y un agente
externo que ejerza una fuerza central. (En el modelo de la gravitación universal
de Newton, el peso depende de la distancia entre el objeto y la tierra.)

No obstante, la masa y el peso, al menos a
nivel intuitivo, no son tan distintos porque, se puede sospechar, son
proporcionales; por ejemplo, si para someter un objeto de masa $m$ a una
aceleración $\mathbf{v}'$ hace falta una fuerza $\mathbf{F}$, entonces para
someter un objeto de masa $2m$ a la misma aceleración, hace falta una fuerza
$2\mathbf{F}$. Esta relación es precisamente lo que afirma el segundo postulado
de Newton en los Principia,
\[
\mathbf{F} = m \cdot \mathbf{v}';
\]
si se considera un objeto de masa $m$ que se deja caer desde el reposo, dado que
$\mathbf{F}$ es el peso (la fuerza de la gravedad sobre el objeto), el segundo
postulado de Newton significa que la razón peso-masa $\mathbf{F}/m$ es
simplemente la aceleración que el objeto sufre bajo condiciones de caída libre.
Luego el peso es directamente proporcional a la masa si, y solo si, todos los
objetos caen con la misma aceleración, y este último hecho ya había sido
establecido por Galileo. Por lo tanto, masa y la magnitud del peso son
proporcionales, lo que se escribe matemáticamente como $F = g \cdot m$.

La última pieza para comprender conceptualmente la deducción de Newton
corresponde con el período de un péndulo simple que se mueve con pequeñas
oscilaciones, es decir, el tiempo en el que se completa un ciclo del movimiento.
Este valor está dado por
\[
T = 2\pi\sqrt{\frac{L}{g}},
\] 
donde $L$ es la longitud del péndulo, y $g$ es la constante de proporcionalidad
entre la masa y el peso, a una cierta distancia del centro de la tierra.

En efecto, medidas hechas por el astrónomo francés Jean Richer mostraban que el
período de un péndulo disminuía 2 minutos y 28 segundos por día cuando se
encontraba una latitud \SI{5}{\degree} más al norte de París. Newton interpretó
esto como un indicio de que la distancia entre el centro de la tierra y la
superficie debía ser mayor cerca del ecuador porque entonces la distancia
tierra-péndulo sería menor hacia el norte, haciendo que la fuerza de atracción
sobre la masa fuera mayor en tales latitudes, y como el período del péndulo
disminuye con mayores fuerzas de atracción, entonces el modelo sería totalmente
consistente con los resultados de Richer.
\begin{figure*}[h]
        \centering
        {\phantomsubcaption\label{fig:1a}}
        {\phantomsubcaption\label{fig:1b}}
        \tikz\node[
        inner sep=2pt,
        label={[anchor=south east]north west:\subref{fig:1a}}
        ]
        {\includegraphics[height=0.3\textwidth]{./diagrams/oblate/oblate.pdf}};
        \tikz\node[
        inner sep=2pt,
        label={[anchor=south east]north west:\subref{fig:1b}}
        ]
        {\includegraphics[height=0.25\textwidth]{./diagrams/prolate/prolate.pdf}};
        \caption{
                Dos versiones de la forma de la tierra. \subref{fig:1a}~Los
                seguidores de la teoría Cartesiana sostenían que la tierra
                estaba alargada hacia los polos; \subref{fig:1b}~Newton dedujo
                que la tierra debía elongarse hacia el ecuador.
        }\label{fig:1}
\end{figure*}

Las observaciones de Newton serían acogidas por sus colegas en la Royal Society;
no así, serían puestas en duda en otras partes de Europa, particularmente en
Francia, donde tres generaciones antes, en 1644, Descartes había expuesto su
propia teoría del todo. Su modelo, en el que no ahondaré mucho, se sustentaba a
partir del contacto entre los objetos y la transferencia de momentum entre los
mismos por medio de ``vórtices'' que presumiblemente serían los movimientos
primordiales con que Dios inició el universo, y que se preservaban inmutables en
el tiempo.

Descartes en realidad nunca propuso una forma para la tierra aparte de la
esférica, pero académicos de la Europa continental extrapolarían su teoría para
concluir que la tierra debía ser un elipsoide elongado hacia los polos, en total
contradicción con la visión Newtoniana.

\section{Antecedentes de la expedición}

La visión cartesiana gozaba de mayor acogida entre los académicos europeos en
general. Ha de recordarse que en aquel entonces el sistema de la física era
esencialmente filosófico, con lo cual una teoría basada en las matemáticas, como
lo era la propuesta por Newton, resultaba extraña y, consecuentemente, fue
recibida con escepticismo. Empero, el ostensible dogmatismo que existía en la
época no era la única razón por la cual las ideas de Newton fueron rechazadas en
Francia y el continente europeo.

Para el momento de la publicación de los Principia, la academia de ciencias de
Francia ya empezaba a realizar estudios geodésicos en territorio francés con el
fin de desarrollar mejores mapas. Los franceses esperaban, claro está, que las
observaciones indicaran que el modelo Cartesiano sobre la forma de la tierra era
el correcto.  Las primeras investigaciones geodésicas en Francia fueron llevados
a cabo en 1670 por el astrónomo Jean Picard. No obstante, el rango de tales
medidas no fue suficiente para establecer la verdadera geometría global.  La
realización de posteriores estudios tuvo que postergarse debido al advenimiento
de varios conflictos entre Francia y otras naciones europeas que agotaron los
fondos que pudieran invertirse en la academia de ciencias.

Para mediados de los años 1690, la situación económica de Francia permitió que
los proyectos de geodesia de la academia francesa resurgieran con el liderazgo
de la familia Phélypeaux. En los años subsecuentes, los debates sobre la forma
de la tierra proseguirían.

Fue sólo hasta 1735 que la expedición geodésica partiría con el claro objetivo
de determinar la verdadera geometría de la tierra, bajo la dirección de los
académicos franceses Louis Godin, Pierre Bouguer, y Charles de La Condamine.  En
noviembre de 1735 llegaron a Cartagena, donde se les unieron los oficiales
navales españoles Jorge Juan y Santacilla, y Antonio de Ulloa, cuya presencia se
debía a que el Perú era una colonia española en el momento. Los oficiales
españoles resulatarían siendo una fuerza de unión clave entre los científicos
que conformaron la expedición, puesto que el trabajo conjunto no era una virtud
característica entre los miembros de la sociedad científica francesa, Godin en
particular carecía del liderazgo necesario para mantener la cohesión en el
equipo.

\section{Medida de la longitud de arco}

El objetivo de la expedición geodésica al ecuador era medir la longitud de arco
correspondiente a un grado de latitud en regiones apartadas la una de la otra,
una medición sería efectuada lo más cerca posible del ecuador, mientras que la
otra se realizaría cerca del polo norte. De esta manera se podría deducir si el
radio ecuatorial era mayor al radio polar, o al contrario.

Bajo cualquiera de los dos modelos que se habían propuesto, debe notarse que un
meridiano traza una elipse; por lo tanto, si se toman dos puntos lo
suficientemente cercanos sobre un mismo meridiano, se puede considerar que la
curvatura de la curva entre ellos es constante y exactamente igual a la curvatura
de una circunferencia en el mismo plano del meridiano, cuyo radio se conoce como
el radio de curvatura meridional. En esencia, esta circunferencia posee
localmente la misma curvatura del meridiano.

Dado que el radio de una circunferencia es inversamente proporcional a su
curvatura, en puntos donde la curvatura del meridiano sea mayor, se tendrá una
circunferencia con menor radio, y por lo tanto, por medio de la relación $s =
r\theta$, se tendrá una menor longitud de arco.

\begin{figure}[h]
        \centering
        \includegraphics[width=0.4\textwidth]{./diagrams/radius.pdf}
        \caption{
                Ilustración de la medida de la longitud de arco de
                \SI{1}{\degree} de longitud. Se ilustran las circunferencias
                definidas por el radio de curvatura meridional. En este caso,
                $S_1 > S_2$.
        }
        \label{fig:2}
\end{figure}

Así, pues, sólo restaba medir empíricamente la longitud de arco de dos puntos
aproximadamente sobre un mismo meridiano en diferentes regiones de la tierra.
Con este propósito se utiliza la triangulación, que es una técnica en la cual se
establece una cadena de triángulos uniendo puntos fijos de referencia sobre los
cuales se miden distancias y ángulos para posteriormente usar relaciones
trigonométricas que permitan calcular la distancia total entre dos puntos
deseados, de esta forma se divide el proceso de medición total en varias
mediciones más pequeñas. Los científicos que realizaron estas mediciones también
debieron introducir correcciones por altitud relativa al nivel del mar, puesto
que la curvatura meridional se debía asumir a ese nivel.

\begin{figure}[h]
        \centering
        \includegraphics[height=0.6\textwidth]{./diagrams/triangulation.pdf}
        \caption{Sistema de triangulación usado en la expedición geodésica en el
        Virreinato del Perú.}
        \label{fig:3}
\end{figure}

\section{La expedición hacia ecuador y sus consecuencias}

El proceso de triangulación, que parecía relativamente sencillo sobre el papel,
no fue una materia trivial. Para llevar a cabo su cometido, los científicos
europeos eligieron un lugar cercano a Quito conocido como ``la avenida de los
volcanes'', un amplio valle rodeado de volcanes en el corazón de lo que
actualmente es ecuador.  Usarían las montañas como sus puntos fijos para la
triangulación.

Durante su viaje, los científicos franceses se enfrentaron a situaciones
inesperadas que harían que su viaje se alargara por cerca de diez años. Para
empezar, las condiciones climáticas en la zona donde se pretendían realizar las
mediciones no era particularmente favorables para las observaciones astronómicas
requeridas para establecer latitudes; además, el transporte de los instrumentos
de medición, particularmente del cuadrante, no resultaba fácil en el terreno en
el que se establecieron para su labor, puesto que debían escalar hasta más de
6000 metros sobre el nivel del mar.

Durante la expedición, los fondos con los que contaban para su financiación se
acabaron; afortunadamente La Condamine había traído cartas de cambio que se
lograron reclamar en el lugar, con lo cual el proyecto pudo seguir. No obstante,
a sus dificultades se sumaría la Guerra del Asiento, que estallaría en el caribe
entre España y Gran Bretaña. Tal conflicto se debió en esencia a los roces
comerciales que habían surgido entre las dos naciones en el territorio
americano, puesto que tras la guerra de sucesión española, los británicos tenían
ahora permiso de comerciar esclavos negros en la américa bajo dominio español.

La guerra se extendería a lo largo de las costas de los dominios españoles,
irrumpiendo así con las labores de medición de los franceses. En particular, los
oficiales españoles que los acompañaban, Jorge Juan y Ulloa tuvieron que
presentarse en la batalla para defender Lima, fragmentando la expedición, y por
tanto, ralentizándola todavía más.

Tras 10 largos años de expedición, los científicos lograron su objetivo,
encontrar un sólo parámetro: la longitud de arco de un grado de latitud en el
ecuador. En efecto, este número probó que Newton estaba en lo cierto, puesto que
ya se habían logrado medidas de un grado de latitud en lugares cercanos al polo
norte, y en Francia:
\begin{itemize}
        \item En el círculo ártico, se estableció una longitud de arco de 96.5 millas.
        \item En Francia, se establecieron 69.0 millas.
        \item En el ecuador, se midieron 68.7 millas.
\end{itemize}

El efecto inmediato, una mejoría muy leve en los sistemas de navegación de la
época; pero en el largo plazo, esta expedición marcaría un hito en la historia
en cuanto a la colaboración científica internacional, amén de marcar la manera
en que las américas eran percibidas en Europa. Es muy probable que los
científicos hubieran sido ayudados por habitantes locales, de manera que
tuvieron un contacto significativo y prolongado con la cultura y las costumbres
de aquellas tierras, lo cual por supuesto no pasaría desapercibido. A su
regreso, los franceses transmitirían, además de los resultados cuantitativos,
una narrativa sobre el nuevo e impresionante mundo en el que habían estado; algo
que sirvió de inspiración para futuros expedicionarios europeos, como el alemán
Alexander von Humboldt, y que además avivaría el creciente sentimiento
patriótico que se empezaba a hacer paso en las colonias españolas en América. El
propio Simón Bolívar estaría fascinado con el relato de la expedición geodésica.


\end{document}
